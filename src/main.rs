// main.rs
use dioxus::prelude::*;

const RESET_CSS: &str = include_str!("reset.css");

#[macro_use]
mod macros;

fn main() {
    //dioxus::liveview::launch(app);
    dioxus::desktop::launch(app);
}

#[derive(Clone)]
struct Todo {
    id: u16,
    text: String,
}

impl Todo {
    fn new(text: impl Into<String>) -> Self {
        Self {
            id: rand::random(),
            text: text.into(),
        }
    }
    fn render<'a>(&'a self, todos: &'a UseState<Vec<Self>>) -> LazyNodes {
        rsx!(div {
            button {
                onclick: |_| {
                    let todos_ref = todos.get();
                    let todos_next: Vec<Self> = todos_ref.iter().filter(|item: &&Todo| item.id != self.id).cloned().collect();
                    todos.set(todos_next);
                },
                "X"
            }
            {
                let text  = &self.text;
                rsx!("{text}")
            }
        })
    }
}

fn app(cx: Scope) -> Element {
    let todos = use_state(&cx, || vec![Todo::new("Buy fruit")]);

    let todo_input = use_state(&cx, || "".to_owned());

    let l = todos.len();

    cx.render(rsx!(
        h1 {
            style: tw!("py-2 px-4 bg-green-500"),
            "TodoMVC"
        }
        div {
            "Total items in list: {l}"
        }
        div {
            input {
                value: "{todo_input}",
                oninput: |evt| { todo_input.set(evt.value.clone()) }
            }
            button {
                onclick: |_| {
                    let s = todo_input.get();
                    if s.trim() == "" {
                        return
                    }
                    todos.make_mut().insert(0, Todo::new(s));
                    todo_input.set("".into());
                },
                "Add"
            }
        }
        todos.iter().map(|item| item.render(&todos))

        css!(RESET_CSS)
    ))
}
