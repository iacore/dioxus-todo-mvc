
macro_rules! css {
    ($stylesheet:expr) => {
        {
            let style = $stylesheet;
            rsx!(style { "{style}" })
        }
    };
}

macro_rules! tw {
    ($class_names:expr) => {
        {
            use tailwind_css::TailwindBuilder;
            let mut tailwind = TailwindBuilder::default();
            let (_, a) = tailwind.inline($class_names).unwrap();
            format_args!("{}", a.clone())
        }
    };
}
